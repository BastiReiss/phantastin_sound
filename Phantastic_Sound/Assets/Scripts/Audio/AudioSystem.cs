using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSystem : MonoBehaviour
{

    public float bassThreshold = 0.0002f; // Adjust this value based on your audio clip and preferences
    public Generator sphereGenerator; // Reference to the SphereGenerator script
    public Subwoofer_Collider collider;

    public GameObject Speaker;
    private Animator _animator;

    private AudioSource audioSource;




    // Start is called before the first frame update
    void Start()
    {

        audioSource = GetComponent<AudioSource>();
        _animator = Speaker.GetComponent<Animator>();

        if (audioSource == null)
        {
            Debug.LogError("AudioSource is not assigned!");
            return;
        }

        audioSource.Play(); // Start playing the audio clip
        InvokeRepeating("AnalyzeAudio", 0f, 0.1f);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {

            _animator.Play("Resonate");
        }
    }

    void AnalyzeAudio()
    {
        if (sphereGenerator == null)
        {
            Debug.LogError("SphereGenerator is not assigned!");
            return;
        }
        float[] spectrumData = new float[256]; // Adjust the array size based on your needs
        audioSource.GetSpectrumData(spectrumData, 0, FFTWindow.Hamming);



        // Extract relevant information from the spectrum data
        float bassMagnitude = spectrumData[1];


        //Debug.Log("Bass Magnitude: " + bassMagnitude);
        // Check if the bass magnitude exceeds the threshold
        if (bassMagnitude > bassThreshold)
        {
            // Communicate the beat to the SphereGenerator to create a ball

            sphereGenerator.OnBeatDetected();
            collider.MoveOnBeat();
            _animator.Play("Base Layer.Resonate", 0, 0.25f);
        }
    }
}

//Debug.Log("Bass Magnitude: " + bassMagnitude);
//Debug.Log("Speaker: " + _animator);
