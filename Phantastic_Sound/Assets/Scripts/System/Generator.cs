using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Generator : MonoBehaviour
{
    public GameObject spherePrefab;
    public float bassMagnitude;
    public float initialVelocityMultiplier = 100f;
    public int sphereCount = 0;


    // Start is called before the first frame update
    void Start()
    {


    }


    // Update is called once per frame
    void Update()
    {

    }

    public void OnBeatDetected()
    {
        // Instantiate sphere with an initial velocity
        GameObject sphere = Instantiate(spherePrefab, transform.position, Quaternion.identity);
        Rigidbody sphereRb = sphere.GetComponent<Rigidbody>();

        // Use bassMagnitude or other FFT analysis results to determine initial velocity
        float initialVelocity = bassMagnitude * initialVelocityMultiplier;

        // Apply the initial velocity to the sphere
        sphereRb.velocity = new Vector3(initialVelocity, 0f, 0f);
        sphereCount++;
        //Debug.Log("Sphere Count: " + sphereCount);
    }
}
