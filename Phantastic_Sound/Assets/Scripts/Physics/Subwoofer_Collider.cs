using UnityEngine;

public class Subwoofer_Collider : MonoBehaviour
{
    public Vector3 defaultPosition = new Vector3(0f, -5f, 18f);
    public Vector3 targetPosition = new Vector3(0f, 2f, 0f); // Adjust the target position as needed
    public float beatDuration = 1f; // Adjust the duration of the movement on the beat

    private float timer = 0f;
    private bool isMoving = false;

    void Update()
    {
        if (isMoving)
        {
            // Increment the timer based on the elapsed time since the last frame
            timer += Time.deltaTime;

            // Calculate the interpolation factor based on the timer and beat duration
            float t = Mathf.Clamp01(timer / beatDuration);

            // Use Bézier curve formula to interpolate between default and target positions
            Vector3 newPosition = BezierLerp(defaultPosition, targetPosition, t);

            // Update the position of the GameObject
            transform.position = newPosition;

            // If the interpolation is complete, reset the timer and stop the movement
            if (t >= 1f)
            {
                isMoving = false;
                timer = 0f;
            }
        }
    }

    // Call this method to initiate the movement on the beat
    public void MoveOnBeat()
    {
        isMoving = true;
    }

    // Bézier curve interpolation function
    private Vector3 BezierLerp(Vector3 p0, Vector3 p1, float t)
    {
        return Vector3.Lerp(Vector3.Lerp(p0, p1, t), p1, t);
    }
}
